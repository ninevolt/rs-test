import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import PageFilter from '../pages/PageFilter.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: PageFilter
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
