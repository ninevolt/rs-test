
const filters = [
	{
		type: 'check',
		search: true,
		searchTitle: 'Поиск стран',
		param: 'country',
		title: 'Страна',
		
		items: [
			{title: 'Австрия', value: 1},
			{title: 'Азербайджан', value: 2},
			{title: 'Албания', value: 3},
			{title: 'Алжир', value: 4},
			{title: 'Ангола', value: 5}
		]
	},
	{
		type: 'check',
		search: false,
		param: 'type',
		title: 'Тип',
		
		items: [
			{title: 'Апартаменты', value: 1},
			{title: 'Отель', value: 2}
		]
	},
	{
		type: 'check',
		search: false,
		param: 'stars',
		title: 'Категория звёзд',
		
		items: [
			{title: '1 звезда', value: 1},
			{title: '2 звезды', value: 2},
			{title: '3 звезды', value: 3},
			{title: '4 звезды', value: 4},
			{title: '5 звезд', value: 5}
		]
	},
	{
		type: 'reviews',
		param: 'reviews',
		title: 'Количество отзывов (от)',
		caption: 'Например, 10'
	},
	{
		type: 'price',
		param: 'price',
		title: 'Цена',
		min: 0,
		max: 3689
	}
]

const items = [
	{
		id: 1,

		params: {
			country: 1,
			type: 1,
			stars: 1,
			reviews: 18,
			price: 2789
		},

		"name": "Marina Inn",
		"country": "Австрия",
		"address": "Фалираки, Родос, Греция",
		"stars": 1,
		"type": "Апартаменты",
		"description": "Этот 4-звездочный отель расположен в центре города. На его территории есть бассейн с террасой и сауна. Из номеров открывается вид на море или на средневековый город.",
		"services": ["Пляж","Кондиционер","Открытый бассейн","Бесплатная парковка","Бесплатный WiFi","Вид на море","Бесплатный завтрак"],
		"min_price": 2789.00,
		"currency": "RUR",
		"rating": 4.5,
		"reviews_amount": 18,
		"last_review": "Отличное расположение. Вкусный завтрак. Отдыхали с детьми - все понравилось."
	},
	{
		id: 1,

		params: {
			country: 2,
			type: 2,
			stars: 2,
			reviews: 4,
			price: 3245
		},

		"name": "Mondrian Suites",
		"country": "Азербайджан",
		"address": "Фалираки, Родос, Греция",
		"stars": 2,
		"type": "Отель",
		"description": "Из окон открывается вид на город.К услугам гостей номера-студио с балконом и чайником в 2,7 км от пляжа.",
		"services": ["Пляж","Кондиционер","Открытый бассейн","Платная парковка","Бесплатный WiFi","Вид на море"],
		"min_price": 3245.20,
		"currency": "RUR",
		"rating": 5,
		"reviews_amount": 4,
		"last_review": "Потрясающее место, в номере есть все необходимое. Красивый вид на море."
	},
	{
		id: 1,

		params: {
			country: 3,
			type: 1,
			stars: 3,
			reviews: 36,
			price: 1153
		},

		"name": "Sunny Appartments",
		"country": "Албания",
		"address": "Родос, Родос, Греция",
		"stars": 3,
		"type": "Апартаменты",
		"description": "Все номера и апартаменты оборудованы кондиционером и телевизором с плоским экраном. Также в распоряжении гостей тостер и чайник.",
		"services": ["Пляж","Кондиционер","Бесплатная парковка","Бесплатный WiFi"],
		"min_price": 1153.00,
		"currency": "RUR",
		"rating": 2.4,
		"reviews_amount": 36,
		"last_review": "Бассейн очень маленький. Кормят невкусно. Больше не поедем."
	},
	{
		id: 1,

		params: {
			country: 4,
			type: 2,
			stars: 4,
			reviews: 25,
			price: 3689
		},

		"name": "Super All Inclusive Hotel",
		"country": "Алжир",
		"address": "Родос, Родос, Греция",
		"stars": 4,
		"type": "Отель",
		"description": "Все номера оснащены телевизором с плоским экраном. Из некоторых номеров открывается вид на море или город.",
		"services": ["Пляж","Кондиционер","Открытый бассейн","Бесплатный WiFi","Вид на море","Бесплатный завтрак"],
		"min_price": 3689.00,
		"currency": "RUR",
		"rating": 4.1,
		"reviews_amount": 25,
		"last_review": "Недалёко от пляжа и старого города, вокруг много разных магазинчиков"
	},
	{
		id: 1,

		params: {
			country: 5,
			type: 1,
			stars: 5,
			reviews: 0,
			price: 1896
		},
		
		"name": "Adams Hotel",
		"country": "Ангола",
		"address": "Родос, Родос, Греция",
		"stars": 5,
		"type": "Апартаменты",
		"description": "Отель расположен всего в 100 метрах от пляжа и в 5-ти минутах ходьбы от исторической части города, недалеко от всех основных достопримечательностей. Из отеля открывается вид на море. К услугам гостей спокойный открытый бассейн.",
		"services": ["Пляж","Кондиционер","Открытый бассейн","Бесплатная парковка","Бесплатный WiFi","Бесплатный завтрак"],
		"min_price": 1896.00,
		"currency": "RUR",
		"rating": 0,
		"reviews_amount": 0,
		"last_review": ""
	}
]

export {
	filters,
	items
}
