import { InjectionKey } from 'vue'
import { createStore, Store } from 'vuex'
import { filters, items } from '../data/data'

interface Params {
	country: number
	type: number
	stars: number
	reviews: number
	price: number
} 

interface Item {
	id: number
	params: Params
	name: string
	country: string
	address: string
	stars: number
	type: string
	description: string
	services: string[]
	min_price: number
	currency: string
	rating: number
	reviews_amount: number
	last_review: string
}

export interface State {
  filters: object[],
  allItems: Item[],
  items: object[]
}

export const key: InjectionKey<Store<State>> = Symbol()

export const store = createStore<State>({
	state: {
		filters,
		allItems: items,
		items: []
	},

	mutations: {
		items (state, data) {
			state.items = data
		}
	},

	actions: {
		getItems ({ state, commit }, model) {
			const items = state.allItems.filter((el) => {
				let res = true

				for (let key in model) {

					if (!model[key]) continue
						
					if (key === 'country' || key === 'type' || key === 'stars') {
						res = model[key].includes(el.params[key])
					}
					else if (key === 'reviews') {
						res = el.params[key] >= model[key]
					}
					else if (key === 'price') {
						res = el.params[key] >= model[key][0] && el.params[key] <= model[key][1]
					}

					if (!res) break

				}

				return res
			})

			commit('items', items)
		}
	}
})
